## CI + CD jobs catalog used in the demo

### Helm chart building

Use the `chart-build` component to build and sign a Helm chart as an OCI image and store it in the GitLab container registry.

### WIP: Flux manifests building

Use the `flux-oci` component to build and sign a Flux OCI source image and store it in the GitLab container registry.
